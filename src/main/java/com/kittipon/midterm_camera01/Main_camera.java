/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kittipon.midterm_camera01;

import java.util.Scanner;

/**
 *
 * @author kitti
 */
public class Main_camera {

    public static void main(String[] args) {
        Camera camera01 = new Camera();
        Scanner kb = new Scanner(System.in);
        boolean turn = false;
        System.out.println("Type (Y) to turn on this camera.");
        while (turn == false) {
            cameraStatus(turn);
            char status = Input(kb);
            turn = turnOn(status, turn);
        }

        while (true) {
            System.out.println("Type (S) to take a picture");
            System.out.println("Type (D) to Delete a picture");
            System.out.println("Type (F) to turn off this camera");
            char Do = Input(kb);
            if (turnOff(Do)) {
                break;
            }
            camera01.inPut(Do, turn);
        }

    }

    private static void cameraStatus(boolean turn) {
        System.out.print("Camera status : ");
        if (turn == false) {
            System.out.println("OFF");
        } else {
            System.out.println("ON");
        }
    }

    private static boolean turnOff(char Do) {
        if (Do == 'F') {
            System.out.println("Camera turn off");
            return true;
        }
        return false;
    }

    private static boolean turnOn(char status, boolean turn) {
        if (status == 'Y') {
            System.out.println("Camera turn on");
            turn = true;
        } else {
            System.out.println("Error!!!");
        }
        return turn;
    }

    private static char Input(Scanner kb) {
        String str = kb.next();
        return str.charAt(0);
    }
}

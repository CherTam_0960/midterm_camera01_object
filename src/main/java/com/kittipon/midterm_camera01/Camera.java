/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kittipon.midterm_camera01;

/**
 *
 * @author kitti
 */
public class Camera {

    String camera = "CAAMEERAA";
    String Do = "";
    int memory = 10;

    public void inPut(char dosomething, boolean turn) {
        if (dosomething == 'S' && turn == true) {
            Shutter();
        } else if (dosomething == 'D' && turn == true) {
            Delete();
        } else {
            System.out.println("Error!!!");
        }

    }

    public void ShowName() {
        System.out.println("This camera is " + camera + ".");
    }

    public void Shutter() {
        if (memory <= 0) {
            System.out.println("You dont have enough memory now!!");
            System.out.println("Please delete some photo or change your memory");
        } else if (memory <= 5) {
            ShowCanShutter();
            System.out.println("*** Your memory is almost full!!! ***");
            memory--;
            ShowMemory();
        } else {
            ShowCanShutter();
            memory--;
            ShowMemory();
        }

    }

    public void Delete() {
        if (memory < 10) {
            System.out.print("You delete 1 of photo and ");
            memory++;
            ShowMemory();
        } else if (memory >= 10) {
            System.out.print("No have no picture to delete.");
            ShowMemory();
        }

    }

    private void ShowCanShutter() {
        System.out.println("You got that photo");
    }

    private void ShowMemory() {
        System.out.println("now you have " + memory + " photo left.");
    }
}
